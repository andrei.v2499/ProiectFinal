FROM openjdk:11.0.1-jre-slim-stretch
COPY ./target/spring-petclinic-2.7.0-SNAPSHOT.jar /usr/app/
WORKDIR /usr/app
EXPOSE 8090
ENTRYPOINT ["java", "-jar", "spring-petclinic-2.7.0-SNAPSHOT.jar"]
